﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Data.SQLite;
using System.Data;
using System.Globalization;

namespace SQLiteCRUDUsingDG
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private static SQLiteConnection conn = new SQLiteConnection("Data Source=database.db;Version=3;New=True;Compress=True;");

        private static SQLiteDataAdapter da;

        private static SQLiteCommandBuilder cmdBuilder;

        bool hasTableLoaded = false;

        private static string globalTableName = "myTable 3/12/2017 9:54:00 AM";

        public MainWindow()
        {
            InitializeComponent();
            bindMyGrid();
            loadColNameCB();
            List<string> columnTypeList = new List<string>();
            columnTypeList.Add("String");
            //columnTypeList.Add("Int");
            columnTypeCB.ItemsSource = columnTypeList;
            
        }

        private void bindMyGrid()
        {
            try
            {
                conn.Open();
                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = "select * from '"+ globalTableName + "';";
                da = new SQLiteDataAdapter(command.CommandText, conn);
                cmdBuilder = new SQLiteCommandBuilder(da);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGrid.ItemsSource = dt.DefaultView;
                hasTableLoaded = true;
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void submitBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                da.Update((dataGrid.ItemsSource as DataView).Table);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void addColumnBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                conn.Open();
                SQLiteCommand command = conn.CreateCommand();
                command.CommandText = "alter table '" + globalTableName + "' add column '" + columnNameTB.Text + "' varchar(100);";
                command.ExecuteNonQuery();

                command = conn.CreateCommand();
                command.CommandText = "select * from '" + globalTableName + "';";
                da = new SQLiteDataAdapter(command.CommandText, conn);
                cmdBuilder = new SQLiteCommandBuilder(da);
                DataTable sqlDT = new DataTable();
                da.Fill(sqlDT);
                dataGrid.ItemsSource = sqlDT.DefaultView;
                loadColNameCB();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void deleteColumnBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (columnNameDeleteCB.SelectedIndex != -1)
                {
                    string colNameSelected = columnNameDeleteCB.SelectedItem.ToString();

                    List<string> colNames = new List<string>();
                    DataTable table = new DataTable();
                    table = ((DataView)dataGrid.ItemsSource).ToTable();

                    foreach (DataColumn col in table.Columns)
                    {
                        if (col.ToString() != colNameSelected)
                        {
                            colNames.Add(col.ToString());
                        }
                    }

                    conn.Open();

                    DateTime localDate = DateTime.Now;
                    var culture = new CultureInfo("en-US");

                    //Removing date time from global table name
                    string oldTableName = globalTableName;
                    MessageBox.Show("Global table name before: " + globalTableName);
                    globalTableName = globalTableName.Substring(0, globalTableName.IndexOf(" ") + 1);
                    MessageBox.Show("Global table name now: " + globalTableName);

                    //Now global table name is ready for time stamping
                    string newTableName = globalTableName+" " + localDate.ToString(culture);

                    string createString = "create table '" + newTableName + "' (";
                    int count = colNames.Count;
                    foreach (string colName in colNames)
                    {
                        if (count >= 1)
                        {
                            createString += colName;
                            createString += " varchar(100)";
                            count--;
                            if (count >= 1)
                            {
                                createString += ", ";
                            }
                        }
                    }
                    createString += ", Primary Key (ID));";

                    SQLiteCommand command = conn.CreateCommand();
                    command.CommandText = createString;
                    command.ExecuteNonQuery();
                    MessageBox.Show("New Table: " + createString);

                    string dataTransfer = "insert into '" + newTableName + "' select ";
                    count = colNames.Count;
                    foreach (string colName in colNames)
                    {
                        if (count >= 1)
                        {
                            dataTransfer += colName;
                            count--;
                            if (count >= 1)
                            {
                                dataTransfer += ", ";
                            }
                        }
                    }
                    dataTransfer += " from '"+oldTableName+"';";
                    command = conn.CreateCommand();
                    command.CommandText = dataTransfer;
                    command.ExecuteNonQuery();

                    MessageBox.Show("Data Transfer: " + dataTransfer);

                    //Clearing datagrid
                    dataGrid.ItemsSource = null;

                    //Deleting old table
                    command = conn.CreateCommand();
                    command.CommandText = "drop table '" + oldTableName + "'";
                    command.ExecuteNonQuery();
                    

                    //Renaming new table to old table to prevent confusing user when reusing the table
                    command = conn.CreateCommand();
                    command.CommandText = "alter table '" + newTableName + "' rename to '" + oldTableName + "'";
                    command.ExecuteNonQuery();

                    globalTableName = oldTableName;

                    //Repopulating datagrid with new table
                    command = conn.CreateCommand();
                    command.CommandText = "select * from '" + globalTableName + "'";
                    da = new SQLiteDataAdapter(command.CommandText, conn);
                    cmdBuilder = new SQLiteCommandBuilder(da);
                    DataTable tempTable = new DataTable();
                    da.Fill(tempTable);
                    dataGrid.ItemsSource = tempTable.DefaultView;
                    loadColNameCB();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void loadColNameCB()
        {
            if (hasTableLoaded)
            {
                DataTable table = new DataTable();
                table = ((DataView)dataGrid.ItemsSource).ToTable();
                List<string> colNames = new List<string>();
                foreach (DataColumn col in table.Columns)
                {
                    if (col.ToString() != "ID")
                    {
                        colNames.Add(col.ToString());
                    }
                }
                columnNameDeleteCB.ItemsSource = colNames;
            }
        }
    }
}
